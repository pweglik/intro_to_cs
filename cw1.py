def zad1():
    a = 0
    b = 1
    print(a)
    print(b)
    while True:
        b = b + a
        a = b - a
        if b < 1000000:
            print(b)
        else:
            break

def zad2pom(a,b):
    while b < 2020:
        b = b + a
        a = b - a
        if b == 2020:
            return True
    return False


def zad2():
    for i in range(1, 100):
        for j in range(1, i+1):
            if zad2pom(i-j, j):
                print(i, j)
                return True

    return False

def zad3(n):
    a = 0
    b = 1
    c = 0
    d = 1
    sum = 0
    while True:
        if b > n:
            return False

        if sum < n:
            sum += b
            b = b + a
            a = b - a

        elif sum > n:
            sum -= d
            d = d + c
            c = d - c

        else:
            return True

def zad4(k):
    sum = 0
    add = 1
    counter = 0
    while sum < k:
        sum += add
        add += 2
        counter += 1

    return counter



def zad5(a, num_of_iterations):
    result = 1
    prev_result = 1
    for i in range(num_of_iterations):
        prev_result = result
        result = (result + a/result)/2
        if abs(prev_result - result) < 0.00001:
            break

    return result

def zad6(a = 4, b = 5):
    x1 = (a+b)/2
    if x1**x1 - 2020 == 0:
        return x1
    
    if abs(a-b) > 0.0001:
        if (a**a - 2020)*(x1**x1 - 2020) < 0:
            zad6(a,x1)
        else:
            zad6(x1,b)
    else:
        print(x1)
        return x1

def zad7(n):
    a = 0
    b = 1
    while a*b<n:
        b = b + a
        a = b - a
        if a*b == n:
            print(a,b)
            return True
    return False

def zad8(n):
    if n < 2:
        return False
    if n == 2 or n == 3:
        return True
    if n % 3 == 0 or n % 2 == 0:
        return False
    k = 1
    print()
    while (6*k + 1) < n**(1/2):
        if n % (6*k - 1) == 0 or n % (6*k + 1) == 0:
            return False
        k +=1
    return True

def zad9(n):
    div = []

    k = 1
    while k <= n**(1/2):
        if n % k == 0:
            div.append(k)
            
        k +=1

    lendiv = len(div)
    if div[lendiv - 1] == n**(1/2):
        lendiv -= 1
    for i in range(lendiv):
        div.append(int(n/div[lendiv-i-1]))
        
    return div

def zad10(n):
    if n == sum(zad9(n)[:-1]):
        return True
    else:
        return False

def zad11():
    sums = []
    for i in range(1,1000001):
        s = sum(zad9(i)[:-1])
        sums.append(s)
 
    friends = []
    for i in range(0,1000000):
        if(sums[i] < 1000000):
            if sums[ sums[i]-1 ] == i+1 and sums[i] != i+1:
                friends.append([i+1, sums[i]])

    friends.sort(key = lambda x: x[0])
    for friend in friends:
        friends.remove([friend[1], friend[0]])

    return friends

def zad12pom(a, b):
    while a != b:
        if a > b:
            a -= b
        else:
            b -= a
    return a

def zad12(a,b,c):
    return zad12pom(zad12pom(a,b), c)

def zad13(a,b,c):
    return int(a * b * c/zad12pom(a,b)/zad12pom(c,a*b/zad12pom(a,b)))


def zad14pom(n):
    if n == 0:
        return 1
    factorial = 1
    for i in range(2,n+1):
        factorial *= i
    return factorial

def zad14(x, precision):
    last = 0
    new = 1
    counter = 1
    while abs(new-last) > precision:
        print(new)
        last = new
        new += (-1)**counter * x**(counter*2)/zad14pom(counter*2)
        counter += 1
    
    return new

def zad15pom(n):
    if n == 0:
        return 0.5**0.5
    return (0.5 + 0.5*zad15pom(n-1))**0.5

def zad15pom2(n):
    s = 1
    for i in range(0, n):
        s = s * zad15pom(i)
    return s

def zad15(precision):
    counter = 1
    new = 1
    last = 0
    while abs(new-last) > precision:
        last = new
        new = 2/zad15pom2(counter)
        counter += 1
    return new

def zad16pom(a):
    steps = 0
    while a != 1:
        steps += 1
        a = (a % 2)*(3*a+1)+ (1- a%2)*a/2

    return steps
    


def zad16():
    longest = 0
    index = None
    for i in range(2,10001):
        new = zad16pom(i)
        if new > longest:
            longest = new
            index = i
    
    return (index,longest)

def zad17():
    a = 0
    b = 1
    while True:
        b = b + a
        a = b - a
        if b < 10000:
            print(b/a)
        else:
            break
    return b/a


def zad18(a, num_of_iterations):
    result = 1
    prev_result = 1
    for i in range(num_of_iterations):
        prev_result = result
        result = result - (result**3 - a)/(3*result**2)
        if abs(prev_result - result) < 0.00001:
            break
    return result

def zad19(precision):
    last = -1
    new = 0
    counter = 0
    while abs(new-last) > precision:
        print(new)
        last = new
        new += 1/zad14pom(counter)
        counter += 1
    
    return new

def zad20(a, b, precision):
    last = (0,0)
    new = (a,b)
    while abs(new[0] - new[1]) > precision:
        last = new
        new = ((last[0]*last[1])**0.5, (last[0]+last[1])/2)
        print(new)
    
    return new[0]






if __name__ == '__main__':
    n = int(input())
    print(zad8(n))

