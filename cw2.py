import math


def zad1():
    n = int(input())
    a = 0
    b = 1
    
    while b < n:
        c = 0
        d = 1
        if b * d == n:
            return True

        while b * d < n:
            c, d = d, c + d
            if b * d == n and b != d:
                return True
 
        a, b = b, a + b

    return False

def zad2():
    a = int(input())
    b = int(input())
    n = int(input())
    quotient = 0
    if a == b:
        return 1

    if a > b:
        quotient = a//b
        a -= b * quotient

    decimals = str(quotient) + "."
    for i in range(n):
        a *= 10
        quotient = a//b
        a -= b * quotient
        decimals += str(quotient)

    return decimals

def zad3():
    n = int(input())

    stringn = str(n)

    result = [False, False]

    if stringn == stringn[::-1]:
        result[0] = True

    binary = ""

    while n != 0:
        if n % 2 == 0:
            binary += '0'
        else:
            binary += '1'
        n = n//2

    if binary == binary[::-1]:
        result[1] = True

    return result
    
def zad4():
    n = int(input())
    counter = 0
    for i in range(int(math.log(n,2)+1)): 
        for j in range(int(math.log(n,3))+1):
            for k in range(int(math.log(n,5))+1):
                if 2**i * 3**j * 5**k <= n:
                    counter += 1
                    print(2**i,3**j,5**k, 2**i*3**j*5**k)
                

    return counter


def zad5pom(n,nums):
    for i in range(len(n)):
        temp = n[:i]+n[i+1:]
        if len(temp) > 0:
            zad5pom(temp,nums)
            if int(temp) % 7 == 0:
                nums.add(int(temp))
    return 


def zad5():
    n = str(input())
    nums = set()
    zad5pom(n,nums)
    return nums

def zad6():
    n = int(input())

    if n**(0.5) == int(n**(0.5)):
        return str(int(n**(0.5))) + " * " + str(int(n**(0.5)))

    k = int(n**(0.5))
    while True:
        if n % k == 0:
            return str(k) + " * " + str(n//k)
        k -= 1

def zad7():
    n = int(input())

    k = 3
    counter = 4
    while n > k:
        if n % k == 0:
            return True

        k += counter
        counter += 2
    
    return False
        

def zad8pom(n):
    a = 0
    b = 1
    c = 0
    d = 1
    sum = 0
    while True:
        if b > n:
            return False

        if sum < n:
            sum += b
            b = b + a
            a = b - a

        elif sum > n:
            sum -= d
            d = d + c
            c = d - c

        else:
            return True

def zad8():
    n = int(input())

    n += 1

    while zad8pom(n) == True:
        n += 1

    return n

def zad9():
    k = float(input())
    divisions = 30
    new = 1
    last = 0

    while abs(new - last) > 0.00001:
        print(new)
        last = new
        dx = (k-1)/divisions
        new = 0
        for i in range(divisions):
            new += dx/(1 + i*dx)
        divisions += 1
    
    return new


def zad10():
    n = int(input())
    a = 2

    while n >= a:
        if n % a == 0:
            return True
        a = 3*a + 1
    return False
        

def zad11():
    n = int(input())

    previous = 0

    while n > 0:
        if (n % 10) <= previous:
            return False

        previous = n % 10
        n = n//10
    
    return True


def zad12():
    n = int(input())

    length = len(str(n))
    while n > 0:
        digit = n % 10

        if n % 10 == length:
            return True
   
        n = n//10

    
    return False

def zad13():
    n = int(input())

    digits = [0]*10
    last = n % 10
    while n > 0:
        digit = n % 10
        digits[digit] += 1

        n = n//10

    if digits[last] == 1:
        return True
    else:
        return False








if __name__ == '__main__':
    print(zad8())