from math import log10
import random
def zad1(n, base):
    digits = "0123456789ABCDEF"
    result = ""
    while n > 0:
        result = digits[n%base] + result
        n //= base
    return result

def zad2(a, b):
    tab1 = [0]*10
    tab2 = [0]*10

    for i in range(int(log10(a))+1):
        tab1[a%10] += 1
        a //= 10

    for i in range(int(log10(b))+1):
        tab2[b%10] += 1
        b //= 10

    if tab1 == tab2:
        return True
    else:
        return False

def zad3():
    size = 10000
    numbers = [x for x in range(2,size+1)]

    for i in range(len(numbers)):
        if numbers[i] != 0:
            for j in range(i+1,size-1):
                if numbers[j] % numbers[i] == 0:
                    numbers[j] = 0

    
    result = []
    for i in range(len(numbers)):
        if numbers[i] != 0:
            result.append(numbers[i])

    return result


def zad4(n):
    pass

def zad5():
    numbers = [0]*10
    while True:
        n = int(input())
        if n == 0:
            break

        temp = [0]*10
        for i in range(10):
            if numbers[i] < n:
                temp[i] = n
                for j in range(i+1, 10):
                    temp[j] = numbers[j-1]
                break
            else:
                temp[i] = numbers[i]

        numbers = temp.copy()

    return numbers[9]

def zad6(n):
    tab = [None]*n
    for i in range(n):
        tab[i] = random.randint(1,1000)
        print(tab[i])

    result = True

    for el in tab:
        while el > 0:
            remainder = el % 10
            if remainder % 2 == 1:
                break
            el //= 10

        if el == 0: 
            result = False
            break

    return result

def zad7(n):
    pass
            


    







if __name__ == "__main__":
    x = int(input())
    # y = int(input())
    print(zad6(x))