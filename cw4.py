from random import randint


def zad9(tab, k):
    n = len(tab)
    size = 2

    while size <= n:
        for i in range (0,n-size):
            for j in range (0,n-size):
                if tab[i][j] * tab[i+size][j] * tab[i][j+size] * tab[i+size][j+size] == k:
                    
                    return (True, (i + size//2, j + size//2), size)
        size += 2
    


    return False



# print("#########################################")
#                     for row in range(n):
#                         for el in range(n):
#                             if row > i and row < i + size and el > j and el < j + size :
#                                 print("." , end = ' ')
#                             elif (row == i or row == i + size )and el > j and el < j + size:
#                                 print("." , end = ' ')
#                             elif (el == j or el == j + size) and row > i and row < i + size:
#                                 print("." , end = ' ')
#                             else:
#                                 print(tab[row][el], end = ' ')
#                         print()



def zad10(arr):
    n = len(arr)
    if n == 0:
        print("Empty array")
        return False

    for row in arr:
        print(row)
    
    rows_found, cols_found = 0b0, 0b0

    for i in range(n):
        for j in range(n):
            if arr[i][j] == 0:
                rows_found = (1 << (i)) | rows_found
                cols_found = (1 << (j)) | cols_found
           


    
    if (rows_found + 1) & n == 0 and (cols_found + 1) & n == 0:
        return True
    else:
        return False




def zad15(arr):
    for row in arr:
        goodRow = True
        for el in row:
            while el > 0:
                digit = el % 10
                if digit == 2 or digit == 3 or digit == 5 or digit == 7:
                    break #there is prime digit, go to next number in the row
                el //= 10
            
            if el == 0: # there is no prime digit in el
                goodRow = False
                break #at least one number doesnt have prime digit, go to next row
        
        if goodRow == True:
            return (True, row)


    return False


if __name__ == "__main__":
    n = int(input())
    k = int(input())
    arr = [[randint(0,9) for _ in range(n)] for _ in range(n)]
    

    for row in arr:
        print(row)

    print(zad9(arr, k))

    
    # ###### ZADANIE 10
    # test_arr1 = [[3, 4, 6, 0],
    #              [3, 4, 1, 0],
    #              [3, 0, 0, 1],
    #              [0, 4, 6, 4]]

    # test_arr2 = [[3, 4, 6, 0],
    #              [3, 4, 0, 6],
    #              [3, 0, 7, 1],
    #              [0, 4, 6, 4]]

    # test_arr3 = [[3, 4, 6, 0, 7, 9],
    #              [3, 4, 4, 6, 7, 9],
    #              [3, 0, 7, 1, 7, 9],
    #              [0, 4, 6, 4, 7, 9],
    #              [0, 4, 6, 4, 0, 9],
    #              [0, 4, 6, 4, 7, 0]]

    # test_arr4 = []


    # print(zad10(test_arr1))
    # print(zad10(test_arr2))
    # print(zad10(test_arr3))
    # print(zad10(test_arr4))