from random import randint


def add_frac(a, b):
    return (a[0] * b[1] + b[0] * a[1], a[1] * b[1])

def sub_frac(a, b):
    return (a[0] * b[1] - b[0] * a[1], a[1] * b[1])

def mul_frac(a, b):
    return (a[0] * b[0], a[1] * b[1])

def div_frac(a, b):
    return mul_frac(a,(b[1],b[0]))

def pow_frac(a, n):
    result = (1,1)
    for _ in range(n):
        result = mul_frac(result, a) 
    return result

def sim_frac(a):
    i = 2
    sign = None
    if ( (a[0] >= 0 and a[1] > 0) or (a[0] < 0 and a[1] < 0) ):
        sign = 1
    else:
        sign = -1

    numerator = abs(a[0])
    denominator = abs(a[1])

    while i < (denominator+1)//2 and numerator != 1:
        if numerator % i == 0 and denominator % i == 0:
            numerator /= i
            denominator /= i

        if numerator % i != 0 or denominator % i != 0:
                i += 1  
    return (sign * int(numerator), int(denominator))

def print_frac(a):
    print(str(a[0]))
    print("-"*max( len(str( a[0] )), len(str( a[0] )) ))
    print(str(a[1]))
    print()

def input_frac(text = ""):
    if text != "":
        text += "\n"
    print(text, end = "")
    a = int(input("Numerator:"))
    b = 0
    counter = 0
    while b == 0:
        b = int(input("Denominator:"))
        if b == 0:
            counter += 1
            print("Hey shithead, denominator cannot be zero! Try one more time!")
        if counter == 3:
            print("Okay, the game is over, I'm aborting now, happy errors!")
            return False

    return (a, b)
        

def solve_equation_in_quotients():
    print("Form of eqution is:\na11 * x + a12 * y = b1 \na21 * x + a22 * y = b2\n\nAll a's and b's can be quotients")
    a11  = input_frac("a11:")
    a12  = input_frac("a12:")
    b1  = input_frac("b1:")
    a21  = input_frac("a21:")
    a22  = input_frac("a22:")
    b2  = input_frac("b2:")

    W = sim_frac( sub_frac( mul_frac(a11, a22), mul_frac(a12, a21) ) )
    Wx = sim_frac( sub_frac( mul_frac(b1, a22), mul_frac(a12, b2) ) )
    Wy = sim_frac( sub_frac( mul_frac(a11, b2), mul_frac(b1, a21) ) )

    if W[0] == 0 and (Wx[0] != 0 or Wy[0] != 0):
        print("The system has no solution")
        return False
    
    elif W[0] == 0 and Wx[0] == 0 and Wy[0] == 0:
        print("The system has infinitely many solutions in form of (x, A + Bx), \nwhere A is equal to:")
        print_frac( sim_frac( div_frac( b1, a12 ) ) )
        print("and B is equal to:")
        print_frac( sim_frac( div_frac( mul_frac( (-1,1), a11), a12 ) ) )

    else:
        print("The system has unique solution.")
        print("x:")
        print_frac( sim_frac(div_frac(Wx, W)) )
        print("y:")
        print_frac( sim_frac(div_frac(Wy, W)) )


def ex3(data): 
    rows = [False] * 100
    cols = [False] * 100
    diag1 = [False] * 199
    diag2 = [False] * 199
    for pos in data:
        if rows[pos[0]] == True or cols[pos[1]] == True or diag1[99 + pos[0] - pos[1]] == True or diag2[pos[0] + pos[1]] == True:
            return False
        else:
            rows[pos[0]] = True
            cols[pos[1]] = True
            diag1[99 + pos[0] - pos[1]] = True
            diag2[pos[0] + pos[1]] = True

    return True


def ex4(arr):
    pass

def ex5(arr):
    pass


def c_add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def c_sub(a, b):
    return (a[0]- b[0], a[1] - b[1])

def c_mul(a, b):
    return (a[0] * b[0] - a[1] * b[1], a[0] * b[1] + a[1] * b[0])

def c_div(a, b):
    if b == (0,0):
        print("Divide by zero error!")
        return False
    b_conjugate = (b[0], -b[1])
    num = c_mul(a, b_conjugate)
    return (num[0] / (b[0]**2 + b[1]**2), num[1] / (b[0]**2 + b[1]**2))

def c_pow(a, n):
    result = (1, 0)
    for _ in range(n):
        result = c_mul(result, a)
    return result

def c_input():
    a = float(input("Input real part:"))
    b = float(input("Input imaginary part:"))

    return (a, b)

def c_print(a):
    if a[1] == 0:
        print(str(a[0]))
    else:
        print(str(a[0]) + " + " + str(a[1]) + "i")

def complex_quadratic_equation():
    a = c_input()
    b = c_input()
    c = c_input()

    delta = c_sub(c_pow(b, 2), c_mul(c_mul((4, 0), a), c))
    print(delta)
    if delta == (0, 0): #whole square root of delta term can be cancelled
        sol = c_mul((-1, 0), c_div(b, c_mul((2, 0), a)))
        print("There is one (double) solution:")
        c_print(sol)

    elif delta[1] == 0: #imaginary part of delta equals 0 so we can simplify
        if delta[0] > 0:
            sqrt_delta = ((delta[0])**(1/2), 0)
        else:
            sqrt_delta = (0, (delta[0])**(1/2))

        sol1 = c_div(c_add(c_mul((-1, 0), b), sqrt_delta), c_mul((2, 0), a))
        sol2 = c_div(c_sub(c_mul((-1, 0), b), sqrt_delta), c_mul((2, 0), a))

        print("There are two solutions:")
        c_print(sol1)
        c_print(sol2)
    
    else: # calculate square root of delta like that:https://en.wikipedia.org/wiki/Complex_number#Square_root


        root_re = ((delta[0] + (delta[0]**2 + delta[1]**2)**(1/2))/2)**(1/2)

        root_im = ((-delta[0] + (delta[0]**2 + delta[1]**2)**(1/2))/2)**(1/2)
        if delta[1] < 0: root_im *= -1

        sqrt_delta = (root_re, root_im)

        sol1 = c_div(c_add(c_mul((-1, 0), b), sqrt_delta), c_mul((2, 0), a))
        sol2 = c_div(c_sub(c_mul((-1, 0), b), sqrt_delta), c_mul((2, 0), a))

        print("There are two solutions:")
        c_print(sol1)
        c_print(sol2)

if __name__ == "__main__":
    # ex1 #################################
    # fr1 = input_frac()
    # fr2 = input_frac()

    # print_frac(fr1)
    # print_frac(fr2)

    # fr1 = sim_frac(add_frac(fr1, fr2))
    # print_frac(fr1)

    # fr2 = sim_frac(sub_frac(fr1, fr2))
    # print_frac(fr2)  

    # fr3 = sim_frac(mul_frac(fr2, fr1))
    # print_frac(fr3) 

    # fr4 = sim_frac(div_frac(fr3, fr1))
    # print_frac(fr4)   

    # fr1 = sim_frac(pow_frac(fr1,5))
    # print_frac(fr1) 

    
    # print_frac(fr1) 

    # print_frac(fr2)   

    # print_frac(fr3)   

    # print_frac(fr4)     

    # ex2 #####################################

    # solve_equation_in_quotients()

    # ex3 ###########################
    # data1 = [[0,0],[1,1],[1,2]]
    # data2 = [[0,0],[1,2],[2,4]]
    # data3 = [[0,0],[0,2],[2,4]]
    # print(ex3(data1))
    # print(ex3(data2))
    # print(ex3(data3))

    # ex4 ###############################

    # (-2)*x^2 + (4)*x + (3) = 0 https://www.wolframalpha.com/input/?i=%28-2%29*x%5E2+%2B+%284%29*x+%2B+%283%29+%3D+0
    # i*x^2 + (1-i)*x + 3 + 2i = 0 https://www.wolframalpha.com/input/?i=i*x%5E2+%2B+%281-i%29*x+%2B+3+%2B+2i+%3D+0
   
    complex_quadratic_equation()

    # print(c_add((2,1), (3,4)))
    # print(c_add((2,0), (5,0)))

    # print(c_sub((2,1), (3,4)))
    # print(c_sub((2,0), (5,0)))

    # print(c_mul((2,1), (3,4)))
    # print(c_mul((2,0), (5,0)))

    # print(c_div((2,1), (3,4)))
    # print(c_div((2,0), (5,0)))

    # print(c_pow((2,0), 3))
    # print(c_pow((2,1), 2))

