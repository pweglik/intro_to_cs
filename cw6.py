import time
from random import randint
def zad4_knight_problem(n):
    path = [] # list for storing moves

    def knight_move(path): # recurrent function
        if len(path) == n**2: # if count of moves is equal to count of squares on the board, return True as this has to be legit path
            return (True, path)

        x, y = path[len(path) - 1] # retrive the last move thus current position

        candidates = [(x+2, y+1), (x+1, y+2), (x-1, y+2), (x-2, y+1), (x-2, y-1),(x-1, y-2),(x+1, y-2),(x+2, y-1)] # generate possible moves

        for move in candidates: # iterate over them
            if move not in path and move[1] >= 0 and move[1] < n and move[0] >= 0 and move[0] < n: # if move isn't in the path yet and is in boundaries
                path.append(move) # append a move to the path

                next_move = knight_move(path) # check all positions resulting from making this move
                if  next_move[0] == True: # this will only be true if we found legit path of length == n**2
                    return next_move

        path.pop() # if we analised all moves from current positions and they were illegal or led to dead end delete that move and return False
        return (False, None)

    result = knight_move([(0,0)])
    if result[0] == True:
        board = [[None]*n for _ in range(n)]

        counter = 0
        for pos in result[1]:
            board[pos[1]][pos[0]] = counter
            counter += 1

        for row in board:
            print(row)

        return result[1]
    else:
        return False
    

def zad12(arr, target):
    n = len(arr)
    def find_n_tuple(current_product, index, number_list):
        
        if index >= n:
            if current_product == target:
                print(number_list)
                return 1

            return 0


        elif current_product <= target:
            temp2 = find_n_tuple(current_product, index + 1, number_list)

            number_list.append(arr[index])
            temp1 = find_n_tuple(current_product * arr[index], index + 1, number_list) 
            number_list.pop()
            
            return temp1 + temp2

        elif current_product > target:
            return 0

    #enddef
       
    count = find_n_tuple(1, 0, [])
    print(count)


def zad29(T, r):
    def center_of_mass(chosen):
        n = len(chosen)
        center = [0]*3
        for i in range(3): # dimensions
            sum_mass = 0
            counter = 0
            for j in range(n): # points in T
                if chosen[j] == True:
                    sum_mass += T[j][i]
                    counter += 1
                

            center[i] = sum_mass/counter
            
        return center

    def distance_from_origin(a):
        return (a[0]**2 + a[1]**2 + a[2]**2)**0.5


    def choose_point(index, count_chosen):
        if count_chosen >= 3:
            if distance_from_origin(center_of_mass(chosen)) <= r:
                return (True, chosen, distance_from_origin(center_of_mass(chosen)))
        
        if index >= n:
            return (False, None)


        chosen[index] = True
        temp = choose_point(index+1, count_chosen + 1)

        if temp[0] == False:
            chosen[index] = False
            temp = choose_point(index+1, count_chosen)
        

        if temp[0] == True:
            return temp

        return (False, None)



    n = len(T)
    chosen = [False]*n
    returned_value = choose_point(0, 0)
    if returned_value[0] == True:
        for i in range(len(returned_value[1])):
            if returned_value[1][i] == True:
                returned_value[1][i] = T[i]
            else:
                returned_value[1][i] = None
    
    return returned_value

if __name__ == "__main__":
    
    # arr = [2, 4, 3, 6, 6, 4, 5, 10, 5, 6, 7, 20]

    # zad12(arr, 24)

    T = [[1,2,30], [0,0,-10], [9,8,2], [10, 0, 0], [4,7,10], [2,0,8]]
    print(zad29(T,7))

    # n = 12
    # sum_time = [0]*n
    
    # for i in range(100):
    #     for j in range(3, n):
    #         time_beg = time.time()
    #         T = [[randint(0,1000000), randint(0,100000), randint(0,100000)] for _ in range(0,j)]

    #         zad29(T, 1000000)

    #         sum_time[j] += time.time() - time_beg

    # for j in range(n):
    #     print(j, sum_time[j]/100)
    
    