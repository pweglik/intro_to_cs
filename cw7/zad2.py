from timeit import timeit

# Zadanie 2
# 2. Zastosowanie listy odsyłaczowej do implementacji
# tablicy rzadkiej. Proszę napisać trzy funkcje:
# – inicjalizującą tablicę,
# – zwracającą wartość elementu o indeksie n,
# – podstawiającą wartość value pod indeks n.

class Node():
    def __init__(self, value = None, next_node = None):
        self.value = value
        self.next = next_node


class SparseMatrix():
    # Constructor takes default value for matrix and list of tuples with entries:
    # [(data1, index1), (data2, index2), ...] 
    # Tuples are put into value field in Node class
    def __init__(self, default, size, arr_of_entries):
        self.default = default
        self.size = size

        # First non-default value
        self.first = Node(arr_of_entries[0], None)

        # Setting cur pointer at first node
        cur = self.first

        # Add next nodes
        for i in range(1,len(arr_of_entries)):
            next_node = Node(arr_of_entries[i], None)
            cur.next = next_node
            cur = next_node
        

    def get_el(self, index):
        cur = self.first
        while True:
            if cur.value[1] == index:
                return cur.value[0]

            if cur.next == None:
                break

            cur = cur.next
        
        return self.default

    def set_el(self, data, index):
        cur = self.first

        while True:

            if cur.value[1] == index:


                cur.value = (data,index)
                return


            if cur.next == None:
                break

            cur = cur.next

        new_node = Node((data, index), None)
        cur.next = new_node


    def index(self, value):

        # Case of default value
        if value == self.default and self.first.value[1] :
            cur = self.first
            last = 0
            while True:
                if cur.value[1] > last:
                    return last

                last += 1

                if cur.next == None:
                    break

                cur = cur.next

            return last + 1

        # Case of non-default value
        cur = self.first
        while True:
            if cur.value[0] == value:
                return cur.value[1]

            if cur.next == None:
                break

            cur = cur.next
        
        return None

    def get_arr(self):
        arr = [None] * self.size
        for i in range(self.size):
            arr[i] = self.get_el(i)

        return arr
            

# Design Pattern - Factory
# Function that checks if constructor gets valid data
def create_sparse_matrix(default, size, arr_of_entries):
    for i in range(len(arr_of_entries)):
        if arr_of_entries[i][1] >= size:
            print("Incorrect index of entry: ")
            print(arr_of_entries[i])
            return None

    return SparseMatrix(default, size, arr_of_entries)

tab = [0]*100000
tab[5000] = 2
tab[56789] = 20

mat = create_sparse_matrix(0, 100000, [(2,5000),(20, 56789)])

print(mat.get_el(5000))
mat.set_el(3,5000)
print(mat.get_el(5000))


# print(timeit(lambda: tab[30], number=10000))
# print(timeit(lambda: mat.get_el(30), number=10000))
# print()

# print(timeit(lambda: tab[99999], number=10000))
# print(timeit(lambda: mat.get_el(99999), number=10000))
# print()

# print(timeit(lambda: tab[56789], number=10000))
# print(timeit(lambda: mat.get_el(56789), number=10000))
# print()

# print(timeit(lambda: tab[56789], number=10000))
# print(timeit(lambda: mat.get_el(56789), number=10000))
# print()

# print(timeit(lambda: tab.index(2), number=10000))
# print(timeit(lambda: mat.index(2), number=10000))
# print()

# print(timeit(lambda: tab.index(20), number=10000))
# print(timeit(lambda: mat.index(20), number=10000))
# print()




