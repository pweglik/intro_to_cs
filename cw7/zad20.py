from random import randint

class Node():
    def __init__(self, value = None, next_node = None):
        self.value = value
        self.next = next_node


class Interval():
    def __init__(self, left, right):
        if left < right:
            self.left = left
            self.right = right
        else:
            self.left = right
            self.right = left

    def print(self):
        print(self.left, self.right)

    def intersection(self, other):
        if other.left < self.left and other.right < self.left \
            or other.left > self.right and other.right > self.right:

            return False
        else: 
            return True

    def merge(self, other):
        if self.intersection(other) == True:
            self.left = min(self.left, other.left)
            self.right = max(self.right, other.right)
            return True
        else:
            return False


def print_list(first):
    cur = first
    while cur.next is not None:
        cur.value.print()

        cur = cur.next
    print()


def merge_intervals(input_first):
    cur_merged = input_first
    while cur_merged.next is not None:
        print("cur merged:", end ='')
        cur_merged.value.print()
        print_list(input_first)
        cur = input_first
        prev = None
        while True:
            if cur_merged is not cur and cur_merged.value.merge( cur.value ) == True:
                if prev is not None:
                    prev.next = cur.next
                else:
                    input_first = cur.next
                print('Delete ', end='')
                cur.value.print()
            prev = cur
            cur = cur.next
            if cur.next is None:
                break

        cur_merged = cur_merged.next
    return input_first


# Create random list of intervals
input_first = Node()
cur = input_first

for _ in range(6):
    temp = randint(1, 30)
    cur.value = Interval(temp, temp + randint(1,7))
    cur.next = Node()
    cur = cur.next

print_list(input_first)

input_first = merge_intervals(input_first)

print_list(input_first)
