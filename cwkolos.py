from random import randint

def zad1(tab, x, y, a, b):
    n = len(tab)

    def cal_sum(tab, x, y, a, b):
        suma = 0
        n = len(tab)
        
        for i in range(n):
            for j in range(n):
                if i == x or i == a or j == y or j == b:
                    suma += tab[i][j]

        if x != a and y != b:
            suma -= tab[x][y] + tab[a][b]

        return suma

    start_suma = cal_sum(tab,x,y,a,b)
    print(start_suma)

    for i in range(n):
        if i != x:
            if cal_sum(tab,i,y,a,b) > start_suma:
                return True,i,y,a,b
        
        if i != y:
            if cal_sum(tab,x,i,a,b) > start_suma:
                return True,x,i,a,b

        if i != a:
            if cal_sum(tab,x,y,i,b) > start_suma:
                return True,x,y,i,b

        if i != b:
            if cal_sum(tab,x,y,a,i) > start_suma:
                return True,x,y,a,i
        

# n = 5
# tab = [[randint(0, 10) for _ in range(n)] for _ in range(n)]
# for row in tab:
#     print(row)

# print(zad1(tab,0,0,0,4))


def zad2():
    t = [0]*9
    t[0] = 1
    chosen = [False]*9
    chosen[0] = True
    count_chosen = 1
    count = 0

    def choose(j):
        nonlocal count_chosen, count
        if count_chosen == 9:
            print(t)
            count += 1
            return

        for i in range(2,10):
            
            if chosen[i-1] == False and abs(t[j-1] - i) >= 2:
                if (t[j-1] in [2,3,5,7] and i in [4,6,8,9]) or t[j-1] in [4,6,8,9] or t[j-1] == 1:
                    t[j] = i
                    chosen[i-1] = True
                    count_chosen += 1
                    choose(j+1)

                    chosen[i-1] = False
                    count_chosen -= 1



    choose(1)
    return count

# print(zad2())

def nwd(a,b):
    while b != 0:
        a = a % b
        a, b = b, a

    return a


def nwd4(a,b,c,d):
    return nwd(nwd(a,b),nwd(c,d))

lista = []
def zad1_1(n, tab):
    for i in range(n-1):
        for j in range(n):
            a = tab[i][j]
            b = tab[i+1][j]
            for k in range(n):
                for l in range(n-1):
                    if k != i and k != i + 1 and l != j and l + 1 != j:
                         c = tab[k][l]
                         d = tab[k][l+1]
                         if nwd4(a,b,c,d) == 1:
                             lista.append((a,b,c,d))

    return False

tab = [[1,2,3,4],
       [5,6,7,8],
       [9,10,11,12],
       [13,14,15,16]]

# zad1_1(4,tab)
# print(lista)

pieces = [(x,y) for x in range(10) for y in range(10) if x != y]

# print(len(pieces), pieces)

def zad2_2(pieces):
    n = len(pieces)
    chosen = [False] * n

    def choose_p(last, length):
        nonlocal chosen, pieces, n

        longest = 0

        for i in range(n):
            if chosen[i] == False and last[1] == pieces[i][0]:
                chosen[i] = True
                
                temp = choose_p(pieces[i], length + 1)
                if  temp > longest:
                    longest = temp
                    # print(length)
                
                chosen[i] = False        

        if longest != 0:
            return longest
        else:
            return length


    longest = 0
    for p in pieces:
        print(p)
        temp = choose_p(p, 1)
        if temp > longest:
            longest = temp


    
    return longest

# print(zad2_2(pieces))


arr = [randint(0,10) for _ in range(10)]
arr1 = [1,2,3,1,5,2,2,2,3]

def zad1_3(arr):
    n = len(arr)
    chosen = [-1] * n
    def rec_choose(k, pointer, equal_sum):
        nonlocal chosen, n, arr

        part_sums = [0]*k
        for i in range(n):
            if chosen[i] != -1:
                part_sums[chosen[i]] += arr[i]

        for part_sum in part_sums:
            if part_sum > equal_sum:
                return False, -1

        if pointer == n:
            equal_sum = part_sums[0]
            for part_sum in part_sums:
                if part_sum != equal_sum:
                    return False, -1
            
            print(chosen)
            return True, k

        for i in range(k):
            chosen[pointer] = i
            temp = rec_choose(k, pointer+1, equal_sum)
            chosen[pointer] = -1
            if temp[0] == True:
                return temp
        
        return False, -1

        

    arr_sum = 0
    for i in range(n):
        arr_sum += arr[i]

    print(arr_sum)
    biggest = 0
    for i in range(2, n):
        print(i)
        if arr_sum % i == 0:
            temp = rec_choose(i, 0, arr_sum // i)[1]
            if temp > biggest:
                biggest = temp
                
    return biggest

# print(arr)
# print(zad1_3(arr1))

t1 = [randint(0,10) for _ in range(20)]
t2 = [randint(0,10) for _ in range(20)]

def zad2_3(t1, t2):
    n = len(t1)
    chosen = [False]*n
    
    def choose_k_el(arr, k, count_chosen, pointer):
        nonlocal n, chosen, possible_sums
        

        if count_chosen == k: #TODO
            set_sum = 0
            for i in range(n):
                if chosen[i] == True:
                    set_sum += arr[i]

            possible_sums.add(set_sum)

            return

        if pointer == n:
            return
        
        chosen[pointer] = True
        choose_k_el(arr, k, count_chosen + 1, pointer + 1)
        chosen[pointer] = False

        choose_k_el(arr, k, count_chosen, pointer + 1)


    biggest = 0
    for i in range(1,n+1):
        possible_sums = set()
        chosen = [False]*n

        choose_k_el(t1, i, 0, 0)
        t1_possible_sums = possible_sums.copy()

        possible_sums = set()
        chosen = [False]*n
        choose_k_el(t2, i, 0, 0)

        print(i)
        print(t1_possible_sums)
        print(possible_sums)
        for el in t1_possible_sums:
            if el in possible_sums:
                biggest = i
                break


    return biggest

# arr1 = [1,2,3,4]
# arr2 = [5,2,2,1]
# t1 = [63, 51, 55, 95, 24, 29, 16, 10]
# t2 = [75, 95, 79, 94, 79, 39, 62, 16]
# print(t1, sum(t1))
# print(t2, sum(t2))
# print(zad2_3(t1,t2))


# II kolokwoium 2019
def dec_to_five(n):
    string = ""
    while n != 0:
        string = str(n%5) + string
        n = n//5

    return string

def zgodne(a ,b):
    a = dec_to_five(a)
    b = dec_to_five(b)

    count_a = count_b = 0

    for i in range(len(a)):
        if a[i] == '0' or a[i] == '2' or a[i] == '4':
            count_a += 1

    for i in range(len(b)):
        if b[i] == '0' or b[i] == '2' or b[i] == '4':
            count_b += 1

    if count_a == count_b:
        return True
    else:
        return False


def zgodnosc_tablic(i, j, tab1, tab2):
    count_zgodne = 0
    for y in range(len(tab1)):
        for x in range(len(tab1)):
            if i + y > 2 and i + y < len(tab2) + 2 and j + x > 2 and j + x < len(tab2) + 2:
                if zgodne(tab1[y][x], tab2[i-2+y][j-2+x]):
                    count_zgodne +=1

    if count_zgodne/len(tab1)**2 >= 0.33:
        print(i, j)
        return True
    else:
        return False


tab1 = [[1,2],
         [3,4]]
tab2 = [[4,6,8,9],
        [5,8,9,0],
        [1,1,1,1],
        [3,6,8,2]]

def zad2019_1(tab1, tab2):
    max1 = len(tab1)
    max2 = len(tab2)

    for i in range(max1 + max2 - 1):
        for j in range(max1 + max2 - 1):
            if zgodnosc_tablic(i, j, tab1, tab2):
                return True

    return False

# print(zad2019_1(tab1, tab2))


def A(n):
    return n + 3

def B(n):
    return 2*n

def C(n):
    temp = 0
    count = 0
    while n != 0:
        digit = n%10

        if digit % 2 == 0:
            digit += 1

        temp += digit * 10** count
        n = n//10
        count += 1
    
    return temp


def zad2019_2(X, Y, N):

    def op(num, prev, steps, stack):
        nonlocal Y, N
        count = 0
        if steps > N:
            return 0

        if num == Y and steps <= N:
            print(stack)
            count += 1
        
        if prev != 'A':
            stack.append('A')
            count += op(A(num), 'A', steps + 1, stack)
            stack.pop()

        if prev != 'B':
            stack.append('B')
            count += op(B(num), 'B', steps + 1, stack)
            stack.pop()

        if prev != 'C':
            stack.append('C')
            count += op(C(num), 'C', steps + 1, stack)
            stack.pop()

        return count

    return op(X, '', 0, [])


print(zad2019_2(11, 32, 20))


