# Przemysław Węglik
# W funkcji multi iterujemy po kolejnych stringach i wywołujemy is_multistring()
# która zwraca długość napisów wielokrotnych. metoda polega na iterowaniu po
# dlugosci powielajacego sie fragmentu - i, i potem sprawdzaniu kolejnych fragmentów o
# dlugosci i, aby miały dokladnie te same litery co pierwszy fragment
# jesli znajdziemy jeden zly fragment idziemy od razu do dluzszego fragmentu nei ma sensu
# sprawdzac calego string tym podfragmentem
# na poczatku dodalem sprawdzenie czy caly string nie jest ta sama litera, jak specjalny przypadek

def multi(T):
    longest = 0

    for string in T:
        temp = is_multistring(string)
        if temp > longest:
            longest = temp

    return longest



def is_multistring(S):
    result = 0

    is_same_letter = True
    for i in range(1, len(S)):#sprawdzanie dla podciagu powt. o dlugosci 1
        if S[i] != S[0]:
            is_same_letter = False
            break

    if is_same_letter == True:
        return len(S)

    for i in range(1, len(S)+1//2): # iteracja po dlugosciach podciagow
        j = i
        while j + i - 1 < len(S):#iteracja po starcie kolejnego fragmentu
            is_fragment_right = True
            for k in range(j, j + i-1):# porownywanie fragmentu z pierwszy mfragmentem z poczatku stringu
                if S[k] != S[k-j]:
                    is_fragment_right = False
                    break

            if is_fragment_right == False:
                break
            
            j += i
        
        if j >= len(S):
            result = i

    if result == 1:
        return 0
    return len(S)

