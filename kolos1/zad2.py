# Przemysław Węglik
# sub-bin to odejmowanie binarne
# iterujemy po kolejnych parach wierszy liczac ich roznice
# i sprwadzajac czy jest najwieksza roznica po czy mzwraca te roznice w formie binarnej tablicy


def distance(T):
    longest = [0]*len(T[0])
    for i in range(len(T)):
        for j in range(i+1,len(T)):
            result = sub_bin(T[1], T[0])
            if porownaj(result, longest) == 0:
                longest = result

    return result


def porownaj(row1, row2):

    for i in range(len(row1)):
        if row1[i] == 1 and row2[i] == 0:
            return 0
        elif row1[i] == 0 and row2[i] == 1:
            return 1


def sub_bin(row1, row2):
    longest = None
    for i in range(len(row1)):
        if row1[i] == 1 and row2[i] == 0:
            longest = 1
            break
        elif row1[i] == 0 and row2[i] == 1:
            longest = 2
            break

    if longest == 1:
        for i in range(len(row2)):
            if row2[i] == 0:
                row2[i] = 1
            else:
                row2[i] = 0

        j = len(row2) - 1
        temp = row2[j] + 1
        if temp == 1:
            row2[j] = 1
        
        while temp != 1:
            row2[j] = 0
            temp = row2[j - 1] + 1
            j -= 1
            if j < 0:
                break

    if longest == 2:
        for i in range(len(row1)):
            if row1[i] == 0:
                row1[i] = 1
            else:
                row1[i] = 0

        j = len(row1) - 1
        temp = row1[j] + 1
        if temp == 1:
            row1[j] = 1
        
        while temp != 1:
            row1[j] = 0
            temp = row1[j - 1] + 1
            j -= 1
            if j < 0:
                break
    
    print(row1)
    print(row2)

    carry = 0
    result = [0] * len(row1)
    for i in range(len(row1)):

        temp = row1[len(row1) - 1 - i] + row2[len(row1) - 1 - i] + carry
        if temp > 1:
            result[len(row1) - 1 - i] = temp % 2
            carry = 1
        else:
            result[len(row1) - 1 - i] = temp
            carry = 0


    return result
    
