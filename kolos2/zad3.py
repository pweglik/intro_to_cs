# Przemysław Węglik
# funkcja pomocnicza calc_attacked() oblciza sumę pól szachowanych(atakowanych) przez jedną wieżę.
# Sumuje cały wiersz i całą kolumnę a potem odejmuję pole na którym stoi wieża 2 razt (ponieważ zostało dodane i przy liczeniu kolumny i wiersza)
# W funkcji chess() inicjalizuję zmienną biggest na 0 i wniej będe pamiętał największą
# sumę możliwą do uzyskania.
# potem iteruję po wszsytkich możliwych ustawieniach wież: (i,j) i (x,y)
# Liczę dla nich sumy i jeśli nie stoją ani w tym samym wierszu ani kolumnie
# to odejmuję tylko dwa pola które były podwójnie szachowane.
# W przypadku gdy stoją w jednym wierszu lub w jednej kolumnie (nigdy nie mogą stać na raz na jednym polu)
# musimy odjać sumę całego wiersza lub kolumny. Wtedy każde pole poza tymi na których stoją wieże bedzie szchowane raz
# a to z wieżami 0 razy.

def calc_attacked(T,x,y):
    sum_squares = 0
    for i in range(len(T)):
        sum_squares += T[x][i]
        sum_squares += T[i][y]

    sum_squares -= 2 * T[x][y]
    return sum_squares


def chess(T):
    biggest = 0
    biggest_tuple = None
    n = len(T)

    for i in range(n):
        for j in range(n):
            for x in range(n):
                for y in range(j + 1, n):
                    sum_squares = 0
                    sum_squares += calc_attacked(T,i,j) + calc_attacked(T,x,y)

                    if i != x and j != y:
                        sum_squares -= T[i][y] + T[x][j]

                    elif i == x:
                        for index in range(len(T)):
                            sum_squares -= T[i][index]
                        

                    elif j == x:
                        for index in range(len(T)):
                            sum_squares -= T[index][j]
                        
                    
                    if sum_squares > biggest:
                        biggest = sum_squares
                        biggest_tuple = (i,j,x,y)

    return biggest_tuple
