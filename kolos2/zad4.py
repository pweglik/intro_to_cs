# Przemysław Węglik
# Funkcja pomocnicza is_prime sprawdza czy liczba jest pierwsza: kod - oczywisty
# funkcja divide zawiera podfunkcję rekurencyjną reku()
# Warunkiem wyjscia z rekurencji jest: liczba kaałków (count) musi być pierwsza, liczba pozostała do pocięcia (N_left) == 0
# i obecny kawałek (piece) == 0. Tylko wtedy funkcja może zwórić prawdę - wtedy jest to poprawne pocięcie
# Jeśli obecnie odcinany piece jest już liczbą pierwszą to możemy go odciąć i pownownie wywołać rekurencję od nowa (ze zmniejszonym N i zwikeszoną liczbą kawałków) 
# Potem niezaleznie czy był pierwszy czy nie wywołujemy rekurencje doklejajac do obecnego kawałka kolejną liczbę z N_left
# algorytm działa od prawej strony liczby N (od liczb na niższych pozycjach w sys. dziesiętnym)
# jeśli okaze się, że N_left == 0 a podrekurencja keidy piece był liczbą pierwszą nie zwróci true
# to funkcja musi zwróić False ponieważ n ie ma już żadnych liczb które może dokleić do piece




def is_prime(n):
    if n == 2 or n == 3:
        return True
    if n < 2 or n % 2 == 0:
        return False
    i = 3
    while i <= int(n**0.5):
        if n % i == 0:
            return False
        i += 2
    return True


def divide(N):

    def reku(piece, piece_digit, N_left, count):
        temp = False
        if is_prime(count) == True and N_left == 0 and piece == 0:
            return True
        

        if is_prime(piece):
            
            temp = reku(0, 0, N_left, count+1)
            if temp == True:
                return True

        if N_left == 0:
            return False

        piece = piece + (N_left % 10) * 10**piece_digit
        N_left = N_left//10

        return reku(piece, piece_digit + 1, N_left, count)

        

    return reku(0, 0, N, 0)

