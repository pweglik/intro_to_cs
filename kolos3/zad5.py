#Przemysław Węglik
# Uwaga na początek - zadanie napisane zgodnie z treścią zadania, a nie zgodnie z przykładami
# aby diałało z przykładami w ifach powinien być warunek na sprawdzenie nwd każdej pary liczb w trójce
# nwd() i nwd3() to oczywiste funkcje pomocnicze
# ponieważ liczby mogą mieć między sobą co najwyżej jedną przerwę mozęmy rozważyć wszystkie przypadki 
# zamiast zagnieżdżać pętle. Przypadki XXX, XX.X, X.XX, X.X.X, gdzie X to liczba z trójki a . to przerwa
# W takim razie iterujemy po wszystkich liczbach z tablicy i sprawdzamy te 4 kombinacje dla każdej z nich sprawdzając by nie wyjść 
# poza zakres tablicy jednocześnie


def nwd(a, b):
    while b != 0:
        temp = a
        a = b
        b = temp%b
    return a

def nwd3(a,b,c):
    return nwd(nwd(a,b),c)

def trojki(tab):
    length = len(tab)
    found = 0
    for i in range(length):
        if i+2 < length and nwd3(tab[i],tab[i+1],tab[i+2]) == 1:
            found += 1

        if i+3 < length and nwd3(tab[i],tab[i+1],tab[i+3]) == 1:
            found += 1

        if i+3 < length and nwd3(tab[i],tab[i+2],tab[i+3]) == 1:
            found += 1

        if i+4 < length and nwd3(tab[i],tab[i+2],tab[i+4]) == 1:
            found += 1

    return found

