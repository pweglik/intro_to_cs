class Node():
    def __init__(self,val):
        self.val = val
        self.next = None


def iloczyn(z1, z2, z3):
    cur1 = z1
    cur2 = z2
    cur3 = z3
    return_list = Node(-1)
    cur_return = return_list
    prev_return = None
    while True:
        if cur1.val == cur2.val and cur1.val == cur3.val and cur2.val == cur3.val:
            cur_return.val = cur1.val
            new = Node(-1)
            cur_return.next = new
            prev_return = cur_return
            cur_return = cur_return.next
            cur1 = cur1.next
            cur2 = cur2.next
            cur3 = cur3.next
        else:
            if cur1.val == cur2.val and cur3.val > cur2.val and cur1.next != None and cur2.next != None:
                cur1 = cur1.next
                cur2 = cur2.next

            elif cur1.val == cur3.val and cur2.val > cur1.val and cur1.next != None and cur3.next != None:
                cur1 = cur1.next
                cur3 = cur3.next

            elif cur3.val == cur2.val and cur1.val > cur2.val and cur3.next != None and cur2.next != None:
                cur3 = cur3.next
                cur2 = cur2.next

            elif cur1.val < cur2.val and cur1.val < cur3.val and cur1.next != None:
                cur1 = cur1.next

            elif cur2.val < cur1.val and cur2.val < cur3.val and cur2.next != None:
                cur2 = cur2.next
            
            elif cur3.val < cur2.val and cur3.val < cur1.val and cur3.next != None:
                cur3 = cur3.next
            else:
                prev_return.next = None
                break
                
    return return_list


z1 = Node(1)
l = Node(2)
z1.next = l
k = Node(5)
l.next = k
l = Node(6)
k.next = l
k = Node(7)
l.next = k

z2 = Node(1)
l = Node(5)
z2.next = l
k = Node(6)
l.next = k
l = Node(8)
k.next = l
k = Node(10)
l.next = k

z3 = Node(1)
l = Node(4)
z3.next = l
k = Node(5)
l.next = k
l = Node(6)
k.next = l
k = Node(10)
l.next = k


cur = z1
while cur != None:
    print(cur.val, end = ' ')
    cur = cur.next
print()

cur = z2
while cur != None:
    print(cur.val, end = ' ')
    cur = cur.next
print()

cur = z3
while cur != None:
    print(cur.val, end = ' ')
    cur = cur.next
print()

lista = iloczyn(z1,z2,z3)

cur = lista
while cur != None:
    print(cur.val, end = ' ')
    cur = cur.next
print()