# Czy liczba jest pierwsza
def is_prime(a):
    if a == 2 or a == 3:
        return True
    if a < 2 or a % 2 == 0:
        return False
    i = 3
    while i <= int(a**0.5):
        if a % i == 0:
            return False
        i += 2
    return True
# Z dziesiątkowego na binarny
def to_bin(a):
    res = ""
    def rekur(a):
        nonlocal res
        if a == 0:
            return
        rekur(a//2)
        res += str(a%2)
    rekur(a)
    return res
# Z dziesiątkowego na dowolony
def to_base(a, system):
    tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"]
    res = ""
    def rekur(a):
        nonlocal res, system, tab
        if a == 0:
            return
        rekur(a//system)
        res += str(tab[a%system])
    rekur(a)
    return res
# Z binarnego na dziesiętny
def to_dec(a):
    a = str(a)
    res = 0
    for i in range(len(a)):
        if a[i] == "1":
            res += 2**(len(a)-1-i)
    return res
    
# NWW
def nww(a, b):
    return (a*b)//nwd(a, b)
# Czynniki pierwsze
def czynniki(a):
    res = [None for _ in range(a//2)]
    i = 2
    while i <= a:
        if a % i == 0:
            res[i-2] = i
        while a % i == 0:
            a //= i
        i += 1
    return [j for j in res if j != None]
